
WHAT IS iCandy?
------------------------

 iCandy is a clean theme with a div based, fixed width, 2-column layout (content+right sidebar). 


INSTALLATION
------------

 1. Download iCandy from http://drupal.org/project/icandy

 2. Unpack the downloaded files, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configurationl
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

FURTHER READING
---------------

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide
